#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>
#include <map>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

     /* Ingredient& operator=(Ingredient &ingredient) {
          static Ingredient ingredienteFinale;
          ingredienteFinale.Name = ingredient.Name;
          ingredienteFinale.Description = ingredient.Description;
          ingredienteFinale.Price = ingredient.Price;
          return ingredienteFinale;
          };*/
  };

  class Pizza {
    public:
      string Name;
      //list<Ingredient> ingredientsPizza; //l'ho messo io!!!!!!!!!!!!!!!!!
      // e se mettessi una mappa?????????? Al poato dei vari ingredientsPizzs
      // ovvero al posto delle liste!!!!!!!!!!!!
      //vector<Ingredient> ingredientsPizza;
      int totalPrice;
      int ingredientsNumber;
      vector<Ingredient> ingredientsPizza;

      void AddIngredient(const Ingredient& ingredient) {ingredientsPizza.push_back(ingredient);};
      int NumIngredients() const{return ingredientsNumber;};
      int ComputePrice() const{return totalPrice;};
};

  class Order {
    public:
      int numOrdine;
      int numPizzeOrdine;
      int totalPriceOrder;
      vector<int> priceSinglePizzas;
      vector<string> pizzas;
      vector<Pizza> pizzasOrder;

      void InitializeOrder(int numPizzas) {numPizzeOrdine = numPizzas;};
      void AddPizza(const Pizza& pizza) {pizzasOrder.push_back(pizza);};
      const Pizza& GetPizza(const int& position) const {return pizzasOrder[position];};
      int NumPizzas() const {return pizzas.size();};
      int ComputeTotal() const {return totalPriceOrder;};
  };

  class Pizzeria {
    public:
      list<Ingredient> ingredients;
      unordered_map<string, vector<string>> menuPizze;
      vector<Order> totalOrders;
      //map<string, Ingredient> ingredients;

      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);;
      const Ingredient& FindIngredient(const string& name) const;;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
