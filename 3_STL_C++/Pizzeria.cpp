#include "Pizzeria.h"
#include <string>
#include <list>
namespace PizzeriaLibrary {

//list<Ingredient> ingredients;
//list<Ingredient> ingredientsPizza;
//unordered_map<string, vector<string>> menuPizze;


//PER IL MENU PUO SERVIRE UN MAPPA CON CHIAVI???????????????????
//VISTO CHE DEVO AGGIUNGERE ANCHE UNO O PIU INGREDIENTI!

// RICONTROLLARE DOVE METTERE LE LISTE DI INGREDIENTI ED INGREDIENTI PIZZA!!!!!!!
Pizzeria pizzeria;
Pizza pizza;

void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {

    Ingredient ingredient;
    ingredient.Name = name;
    ingredient.Description = description;
    ingredient.Price = price;
    int inserito = 0;
    //unsigned int i = 0;
    //map<string, Ingredient>::iterator it;
    for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
    {
       if (it->Name == ingredient.Name && it->Description == ingredient.Description && it->Price == ingredient.Price)
       {
           inserito = 1;
           throw runtime_error("Ingredient already inserted");

       }
    }
    if(inserito == 0)
    {
        pizzeria.ingredients.push_back(ingredient);
    }
  /*  for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
    {
       std::cout<<it->Name ;

       std::cout<<' ';
       std::cout<<it->Description ;
       std::cout<<' ';
       std::cout<<it->Price;
       std::cout<<'\n';
    }
    std::cout<<inserito;
    std::cout<<'\n';
*/

   /* if(ingredients.count(name) == 0) //non c'è l'ingrediente
    {
        ingredients.insert(pair<string, Ingredient>(name, ingredient));
    }
    else
    {
        throw runtime_error("Ingredient already inserted");
    }
*/
    //l'ho messo per far partire i test del TestOrder.
    menuPizze.clear();

}

const Ingredient &Pizzeria::FindIngredient(const string &name) const {
    static Ingredient ingredient; /*ho dovuto inserire static per farlo funzionare:
                                    vedi i preferiti di Chrome!! : c++ using c strings*/
    bool trovato = false;
  /* if(ingredients.count(name) == 1) //c'è l'ingrediente
    {
       // trovato = true;
        ingredient = ingredients.find(name)->second;
    }
    else
    {
        throw runtime_error("Ingredient not found");
    }*/

    for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
    {
       if (it->Name == name)
       {
           ingredient.Name = name;
           ingredient.Description = it->Description;
           ingredient.Price = it->Price;
           trovato = true;
       }
    }
    if (trovato == false)
    {
       throw runtime_error("Ingredient not found");
    }

    return ingredient;
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients) {

    if((menuPizze.count(name) == 0)) //ovvero, non trovo nessun elemento con quella chiave.
    {
        menuPizze.insert(pair<string, vector<string>>(name, ingredients));
    }
    else
    {
        throw runtime_error("Pizza already inserted");
    }
}

const Pizza &Pizzeria::FindPizza(const string &name) const {
    static Pizza pizza;
    pizza.totalPrice = 0;
    pizza.ingredientsNumber = 0;
    //pizza.ingredientsPizza.reserve(100);
    //vector<Ingredient> ingredientiPizza;
    vector<string> nomiIngredientiPizza;

    unsigned int i = 0;
    bool trovato = false;
    if(menuPizze.count(name) == 1)
    {
        nomiIngredientiPizza = menuPizze.find(name)->second;
        pizza.Name = name;
        trovato = true;
    }
    if(trovato == false)
    {
        throw runtime_error("Pizza not found");
    }
    else
    {
      for(i = 0; i < nomiIngredientiPizza.size(); i++)
      {
        for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
        {
           if (it->Name == nomiIngredientiPizza[i])
           {
               //ingredientiPizza.push_back(*it);
               pizza.totalPrice = pizza.totalPrice + it->Price;
               pizza.ingredientsNumber = pizza.ingredientsNumber + 1;
               pizza.AddIngredient(*it);
           }
        }
      }
    }

  /*
    std::cout<<ingredientiPizza.size();
    std::cout<<ingredientiPizza[0].Name;
    std::cout<<ingredientiPizza[0].Description;
    std::cout<<ingredientiPizza[0].Price;
    std::cout<<ingredientiPizza[1].Name;
    std::cout<<ingredientiPizza[1].Description;
    std::cout<<ingredientiPizza[1].Price;
    */
  /*  for(unsigned int j = 0; j < ingredientiPizza.size(); j++)
    {
        pizza.ingredientsPizza[j].Name = ingredientiPizza[j].Name;
        pizza.ingredientsPizza[j].Description = ingredientiPizza[j].Description;
        pizza.ingredientsPizza[j].Price = ingredientiPizza[j].Price;
    }
    //std::cout<<pizza.Name;*/
    return pizza;
}

int Pizzeria::CreateOrder(const vector<string> &pizzas) {
    if(pizzas == vector<string>())
    {
        throw runtime_error("Empty order");
    }
    /*int numOrdine = 1000 + totalOrders.size(); //il numero degli ordini cresce
    totalOrders.insert((pair<int, vector<string>>(numOrdine, pizzas)));*/
    Order newOrder;
    //vector<Ingredient> ingredientiPizza;
    newOrder.numOrdine = 1000 + totalOrders.size();
    vector<vector<string>> nomiIngredientiPizza;
    vector<int> pricePizzas;
    int totalPriceOrder = 0;
    for(unsigned int i = 0; i < pizzas.size(); i++)
    {
       nomiIngredientiPizza.push_back(menuPizze.find(pizzas[i])->second);
    }

    for(unsigned int k = 0; k < nomiIngredientiPizza.size(); k++)
    {
       pricePizzas.push_back(k);
       pricePizzas[k] = 0;
       for(unsigned int n = 0; n < nomiIngredientiPizza[k].size(); n++)
       {
          for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
          {
             if (it->Name == nomiIngredientiPizza[k][n])
             {
                pricePizzas[k] = pricePizzas[k] + it->Price;
             }
          }

       }
    }

    pricePizzas.shrink_to_fit(); //libero memoria

    for(unsigned int i = 0; i < pizzas.size(); i++)
    {
        newOrder.pizzas.push_back(pizzas[i]);
        newOrder.priceSinglePizzas.push_back(pricePizzas[i]);
        totalPriceOrder = totalPriceOrder + newOrder.priceSinglePizzas[i];
       /* for(i = 0; i < pizzas.size(); i++)
        {
           for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
           {
              if (it->Name == nomiIngredientiPizza[i])
              {
                       ingredientiPizza.push_back(*it);
                       pizza.totalPrice = pizza.totalPrice + it->Price;
                       pizza.ingredientsNumber = pizza.ingredientsNumber + 1;
               }
            }
           */
     }

    //aggiungo l'ordine alla lista degli ordini
    newOrder.totalPriceOrder = totalPriceOrder;
    newOrder.numPizzeOrdine = pizzas.size();
    totalOrders.push_back(newOrder);
    return newOrder.numOrdine;
}

const Order &Pizzeria::FindOrder(const int &numOrder) const {
    static Order ordineTrovato;
    int k = 0;
    vector<Pizza> pizzeOrder;
    for(unsigned int i = 0; i < totalOrders.size() && k != -1; i++)
    {
        if(totalOrders[i].numOrdine == numOrder)
        {
            k = -1;
            pizzeOrder.reserve(totalOrders[i].pizzas.size()); //conosco il numero di pizze dell'ordine.
            ordineTrovato.InitializeOrder(totalOrders[i].numPizzeOrdine);
            ordineTrovato.numOrdine = totalOrders[i].numOrdine;
            ordineTrovato.totalPriceOrder = totalOrders[i].totalPriceOrder;
            ordineTrovato.priceSinglePizzas = totalOrders[i].priceSinglePizzas;
            for(unsigned j = 0; j < totalOrders[i].pizzas.size(); j++)
            {
               ordineTrovato.pizzas.push_back(totalOrders[i].pizzas[j]);
              /* //aggiungo anche le pizze per poi calcolare il totale.
               //ordineTrovato.pizzasOrder.push_back(pizzeria.FindPizza(totalOrders[i].pizzas[j]));
               pizzeOrder.push_back(pizzeria.FindPizza(totalOrders[i].pizzas[j]));
               pizzeOrder[j].Name = pizzeria.FindPizza(totalOrders[i].pizzas[j]).Name;
               pizzeOrder[j].totalPrice = pizzeria.FindPizza(totalOrders[i].pizzas[j]).totalPrice;
               pizzeOrder[j].ingredientsNumber = pizzeria.FindPizza(totalOrders[i].pizzas[j]).ingredientsNumber;
              // ordineTrovato.AddPizza(pizza);*/
            }
        }

    }
    if(k == 0)
    {
        throw runtime_error("Order not found");
    }

    //std::cout<<ordineTrovato.numPizzeOrdine;
    /*std::cout<<ordineTrovato.totalPriceOrder;
    std::cout<<ordineTrovato.numOrdine;*/
    return ordineTrovato;
}

string Pizzeria::GetReceipt(const int &numOrder) const {
    Order ordineTrovato = FindOrder(numOrder);
    string receipt;
    for(unsigned int i = 0; i < ordineTrovato.priceSinglePizzas.size(); i++)
    {
        //"- " + name + ", " + price + " euro" + "\n"
        //"  TOTAL: " + total + " euro" + "\n"
        string costoPizza = to_string(ordineTrovato.priceSinglePizzas[i]);
        receipt = receipt + "- " + ordineTrovato.pizzas[i] + ", " + costoPizza + " euro" + "\n";
     }
     string costoTotale = to_string(ordineTrovato.totalPriceOrder);
     receipt = receipt + "  TOTAL: " + costoTotale + " euro" + "\n";
     return receipt;
}


string Pizzeria::ListIngredients() const {
    vector<string> ingredientNames;
    map<string, Ingredient>::iterator it;
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int indiceMinimo = -1;
    string listIngredients;
    for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
    {
       ingredientNames.push_back(it->Name);
    }

    //ordino in ordine alfabetico ingredientNames: uso il selectionSort.

    for (i = 0; i < ingredientNames.size(); i++)
    {
       string minimo;
       minimo = ingredientNames[i];
       indiceMinimo = i;
       for (j = i + 1; j < ingredientNames.size(); j++)
       {
           if(ingredientNames[j] < minimo)
           {
               minimo = ingredientNames[j];
               indiceMinimo = j;
           }
       }
       ingredientNames[indiceMinimo] = ingredientNames[i];
       ingredientNames[i] = minimo;

    }

    for (i = 0; i < ingredientNames.size(); i++)
    {
        for (list<Ingredient>::iterator it = pizzeria.ingredients.begin(); it != pizzeria.ingredients.end(); it++)
        {
            if(it->Name == ingredientNames[i])
            {
                 //converto il prezzo in stringa.
                 string prezzo = to_string(it->Price);
                 listIngredients = listIngredients + it->Name + " - " + "'" + it->Description + "'" +  ": ";
                 listIngredients = listIngredients + prezzo + " euro" + "\n";
            }
         }
     }


    // std::cout<<listIngredients;
    //lo inserisco per fare in modo che partano i test successivi di testPizza.
     pizzeria.ingredients.clear();
     return listIngredients;
}

string Pizzeria::Menu() const {
    //name + " (" + numIngredients + " ingredientsW): " + price + " euro" + "\n"
    string menuStampato;
    int price = 0;
     for (auto it = menuPizze.begin(); it != menuPizze.end(); ++it)
     {
         menuStampato = menuStampato + it->first + " (";
         string numIngredients = to_string(it->second.size());
         menuStampato = menuStampato + numIngredients + " ingredients): ";
         for(unsigned int i = 0; i < it->second.size(); i++)
         {
            for (list<Ingredient>::iterator iter = pizzeria.ingredients.begin(); iter != pizzeria.ingredients.end(); iter++)
            {

                if(it->second[i] == iter->Name)
                {
                   price = price + iter->Price;
                }

             }
         }
         string priceString = to_string(price);
         menuStampato = menuStampato + priceString + " euro";
         menuStampato = menuStampato + "\n";
         price = 0;

     }
     //std::cout<<menuStampato;
     pizzeria.ingredients.clear();
     return menuStampato;
}





/*void Pizza::AddIngredient(const Ingredient &ingredient) {
    Ingredient ingredientPizza;
    ingredientPizza.Name = ingredient.Name;
    ingredientPizza.Description = ingredient.Description;
    ingredientPizza.Price = ingredient.Price;
    int inseritoPizza = 0;
    //unsigned int i = 0;
    for (vector<Ingredient>::iterator iter = pizza.ingredientsPizza.begin(); iter != pizza.ingredientsPizza.end(); iter++)
    {
       if (iter->Name == ingredient.Name && iter->Description == ingredient.Description && iter->Price == ingredient.Price)
       {
           inseritoPizza = 1;
           throw runtime_error("Pizza Ingredient already inserted");

       }
    }
    if(inseritoPizza == 0)
    {
        pizza.ingredientsPizza.push_back(ingredient);
    }
    for (vector<Ingredient>::iterator it = pizza.ingredientsPizza.begin(); it != pizza.ingredientsPizza.end(); it++)
    {
       std::cout<<it->Name ;

       std::cout<<' ';
       std::cout<<it->Description ;
       std::cout<<' ';
       std::cout<<it->Price;
       std::cout<<'\n';
    }
    std::cout<<inseritoPizza;
    std::cout<<'\n';
    std::cout<<pizza.ingredientsPizza.size();
    std::cout<<'\n';

}


}*/




}
