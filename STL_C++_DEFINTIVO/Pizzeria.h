#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>
#include <map>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;

      ///aggiungo il costruttore vuoto
      Ingredient() {Name = "non ancora inserito"; Description = "Non ancora inserita"; Price = 0;}
  };

  class Pizza {
    public:
      string Name;
      vector<Ingredient> ingredientsPizza;

      Pizza() {}
      Pizza(const string& name,const vector<Ingredient>& _ingredientiPizza) : Name(name), ingredientsPizza(_ingredientiPizza) {}

      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const{return ingredientsPizza.size();};
      int ComputePrice() const;

};

  class Order {
    public:
      int numOrdine;
      int totalPriceOrder;
      vector<Pizza> pizzasOrder;

      void InitializeOrder(int numPizzas) {pizzasOrder.reserve(numPizzas);};
      void AddPizza(const Pizza& pizza) {pizzasOrder.push_back(pizza);};
      const Pizza& GetPizza(const int& position) const;;
      int NumPizzas() const {return pizzasOrder.size();};
      int ComputeTotal() const {return totalPriceOrder;};
  };

  class Pizzeria {
    public:
      unordered_map<string, Pizza> menuPizze;
      unordered_map<int, Order> totalOrders;
      map<string, Ingredient> ingredientsPizzeria;

      void AddIngredient(const string& name,
                         const string& description,
                         const int& price);;
      const Ingredient& FindIngredient(const string& name) const;;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
