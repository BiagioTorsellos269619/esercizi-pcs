#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionType = Intersector2D1D::NoInteresection;
    intersectionParametricCoordinate = 4; /////

}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  planeNormalPointer = planeNormal;
  planeTranslationPointer = planeTranslation;
  return;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
  lineOriginPointer = lineOrigin;
  lineTangentPointer = lineTangent;
  return;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  double controllo;
  controllo = lineTangentPointer[0]*planeNormalPointer[0] + lineTangentPointer[1]*planeNormalPointer[1] + lineTangentPointer[2]*planeNormalPointer[2];

  //controllo per verificare se segmento e piano sono coplanari.

  if(controllo > -(toleranceParallelism) && controllo < (toleranceParallelism) )
  {
      //controllo se il segmento è nel piano o parallelo, verificando
      //se il punto appartiene al piano.
      if((planeNormalPointer[0]*lineOriginPointer[0] + planeNormalPointer[1]*lineOriginPointer[1] + planeNormalPointer[2]*lineOriginPointer[2]) == planeTranslationPointer)
      {
          //il segmento è coplanare
          intersectionType = Intersector2D1D::Coplanar;
          return true;
      }
      else
      {
          //il segmento è parallelo al piano
          intersectionType = Intersector2D1D::NoInteresection;
          return false;
      }
  }

  else
  {
      //intersectionParametricCoordinate = planeNormalPointer[0]*planeTranslationPointer[0] + planeNormalPointer[1]*planeTranslationPointer[1] + planeNormalPointer[2]*planeTranslationPointer[2];
      intersectionType = Intersector2D1D::PointIntersection;
      intersectionParametricCoordinate = planeTranslationPointer -(planeNormalPointer[0]*lineOriginPointer[0] + planeNormalPointer[1]*lineOriginPointer[1] + planeNormalPointer[2]*lineOriginPointer[2]);
      intersectionParametricCoordinate = (intersectionParametricCoordinate)/controllo;

      //calcolo il punto di intersezione.

      puntoIntersezione[0] = lineOriginPointer[0] + intersectionParametricCoordinate*lineTangentPointer[0];
      puntoIntersezione[1] = lineOriginPointer[1] + intersectionParametricCoordinate*lineTangentPointer[1];
      puntoIntersezione[2] = lineOriginPointer[2] + intersectionParametricCoordinate*lineTangentPointer[2];
      return true;
  }
}
