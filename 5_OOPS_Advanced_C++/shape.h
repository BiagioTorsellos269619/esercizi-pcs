#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() {X = 0; Y = 0;} //costruttore per inizializzare a 0,.
      Point(const double& x,
            const double& y) {X = x; Y = y;}
      Point(const Point& point) {X = point.X; Y = point.Y;}

      double ComputeNorm2() const { return sqrt((X*X) + (Y*Y)); }

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        //ho implementato la stampa.
        stream << "Point: x = " << point.X << " y = " << point.Y << endl;;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){
          if(lhs.Perimeter() < rhs.Perimeter())
          {
              return true;
          }
          else
              return false;
      }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){
          if(lhs.Perimeter() > rhs.Perimeter())
          {
              return true; /////////////////////
          }
          else
              return false;
      }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){
          if(lhs.Perimeter() <= rhs.Perimeter())
          {
              return true;
          }
          else
              return false;
      }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){
          if(lhs.Perimeter() >= rhs.Perimeter())
          {
              return true;
          }
          else
              return false;
      }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b) {_center = center; _a = a; _b = b;}
      virtual ~Ellipse() { }
      void SetSemiAxisA(const double &a) {_a = a;}; //aggiunti io perche c erano nell uml
      void SetSemiAxisB(const double &b) {_b = b;}; //aggiunti io perche c erano nell uml
      void AddVertex(const Point& point) {_center = point;};
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Point _circleCenter;
      double radius;
      Circle() { }
      Circle(const Point& center,
             const double& radius);
      //double Perimeter() const; // aggiunto io !!!!!1
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() { }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) {points.push_back(point);};
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      Point _p1;
      double _edge;
      TriangleEquilateral(const Point& p1,
                          const double& edge);
      //double Perimeter() const;
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) {points.push_back(p);};

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
      protected:
      Point _p1;
      int _base;
      int _height;
      public:
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4) {}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
      protected:
      Point _p1;
      int _edge;
      public:
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {}
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
