#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      return 2*M_PI*sqrt((_a*_a + _b*_b)/2);
  }

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    double lato1;
    double lato2;
    double lato3;
    lato1 = sqrt((points[0].X - points[1].X)*(points[0].X - points[1].X) + (points[0].Y - points[1].Y)*(points[0].Y - points[1].Y));
    lato2 = sqrt((points[0].X - points[2].X)*(points[0].X - points[2].X) + (points[0].Y - points[2].Y)*(points[0].Y - points[2].Y));
    lato3 = sqrt((points[2].X - points[1].X)*(points[2].X - points[1].X) + (points[2].Y - points[1].Y)*(points[2].Y - points[1].Y));
    perimeter = lato1 + lato2 + lato3;
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge): Triangle(p1 ,Point(p1.X + edge, p1.Y), Point(p1.X + edge/2, p1.Y + edge*sqrt(3)/2)), _p1(p1)
  {
      _p1 = p1;
      _edge = edge;
  }

  /*double TriangleEquilateral::Perimeter() const
  {
      return Triangle::Perimeter();
  }*/


  ///DEVO IMPLEMENTARE IL PERIMETER DI TRIANGLE EQUILATER????



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.push_back(p1);
      points.push_back(p2);
      points.push_back(p3);
      points.push_back(p4);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    double lato1;
    double lato2;
    double lato3;
    double lato4;
    lato1 = sqrt((points[0].X - points[1].X)*(points[0].X - points[1].X) + (points[0].Y - points[1].Y)*(points[0].Y - points[1].Y));
    lato2 = sqrt((points[1].X - points[2].X)*(points[1].X - points[2].X) + (points[1].Y - points[2].Y)*(points[1].Y - points[2].Y));
    lato3 = sqrt((points[2].X - points[3].X)*(points[2].X - points[3].X) + (points[2].Y - points[3].Y)*(points[2].Y - points[3].Y));
    lato4 = sqrt((points[3].X - points[0].X)*(points[3].X - points[0].X) + (points[3].Y - points[0].Y)*(points[3].Y - points[0].Y));
    perimeter = lato1 + lato2 + lato3 + lato4;
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height) : Quadrilateral(p1, Point(p1.X + base, p1.Y), Point(p1.X + base, p1.Y + height), Point(p1.X, p1.Y + height)), _p1(p1)
  {
      _p1 = p1;
      _base = base;
      _height = height;
   }

  Point Point::operator+(const Point& point) const
  {
      //Point sumPoint = Point();
      Point sumPoint = Point(X, Y);
      sumPoint.X = sumPoint.X + point.X;
      sumPoint.Y = sumPoint.Y + point.Y;
      return sumPoint;
  }

  Point Point::operator-(const Point& point) const
  {
      Point diffPoint = Point(X, Y);
      diffPoint.X = - diffPoint.X + point.X;  //////controllare l'implementazione di + e - negli operatori dei punti, confrontandoli con l'esercizio 5 del prof!!!1
      diffPoint.Y = - diffPoint.Y + point.Y;
      return diffPoint;
  }

  Point&Point::operator-=(const Point& point)
  {
     /* ////DA IMPLEMNETARE!!!!!!!!!!!!!!
      Point diffPoint = Point(X,Y);
      static Point returnPoint;
      returnPoint.X = point.X;
      returnPoint.Y = point.Y;
      returnPoint.X = returnPoint.X - diffPoint.X;
      returnPoint.Y = returnPoint.Y - diffPoint.Y;
      return returnPoint;*/
      X -= point.X;
      Y -= point.Y;
      return *this;
   }

  Point&Point::operator+=(const Point& point)
  {
      X += point.X;
      Y += point.Y;
      return *this;
  }

  Circle::Circle(const Point &center, const double &radius) : Ellipse(center, radius, radius), _circleCenter(center)
  {
      ///aggiunto io!!!!!!!!!!!1
  }

  Square::Square(const Point &p1, const double &edge) : Rectangle(p1, edge, edge), _p1(p1)
  {
      ///aggiunto io!!!!!!
  }

}
